#######################################################################
# 2021/05/04 - PsychDOS 0.0.1 - Quick guide as an old-school magazine #
#######################################################################

The "manual" or "quick quide" I previously mentioned is hopefully going
to be styled like an old-school 1980s or 1990s computer magazine. It may
take a while, but hopefully it will also help me find and fix issues
along the way. I also added a TSR for screen captures.
