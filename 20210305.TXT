###################################################################
# 2021/03/05 - PsychDOS 0.0.1 - Getting PsychDOS on an image with #
#                               FreeDOS installed                 #
###################################################################

I have been trying most of the day to get a copy of PsychDOS inside of 
an image (*.img) file with FreeDOS installed and no luck yet. I have 
tried weird things and normal things like mounting a USB in QEMU and 
creating an image file from the USB itself as '-hdb' and 
'-drive format=raw,file=usbcopy.img' and both result in errors when 
copying ('xcopy /E /Y') files from  "D:\PSYCHDOS" to "C:\PSYCHDOS" and I
get the same errors when trying to run PsychDOS from the USB. However, 
'xcopy', when ran, would display a bunch of plain TXT files as being
copied but once it moves on to a binary-like file, this is when it 
fails.

Nevermind, I think I figured-out a way to get files inside of the image 
and that is by using '-hdb fat:rw:'path/to/PSYCHDOS'. However, it treats
the directory like it is a FAT16 (with experimental RW) and therefore, 
you have a 516MB limit. This means if more files are to be added, then I
will have to do it in steps. By the way, that "516MB" is more like 
~400MB from an EXT4 system's perspective.

I finally got PsychDOS on the image file with FreeDOS installed. I 
played around with some Network-related tools and decided to get rid of 
ARACHNE. Also, the Workbench was going to have a Pastebin option, but it
seems as though DOS does not like "-" when using pipes, to which all of 
these cURL-friendly pastebin sites instructions are using. But ending on
a good note, Dillo and Links both seem to load most http and https 
sites; however, due to limitations, do not expect sites to behave 
correctly.
