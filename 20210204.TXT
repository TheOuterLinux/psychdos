##########################################################
# 2021/02/04 - PsychDOS 0.0.1 - Sound Club and WAV files #
##########################################################

I discovered something insteresting today and that is that SCLUB (Sound 
Club for DOS) can inmport 8000Hz WAV files for instruments. This means 
that I may be including a few from projects like MLLS and ZynAddSubFX 
(and/or Yoshimi). SCLUB can play at 44100Hz, so I am not sure how to 
increase the  quality just yet but worse case scenario, the file sizes 
will be small.
